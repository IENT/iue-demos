ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:latest
FROM ${BASE_IMAGE}

RUN conda install --quiet --yes \
		'scipy==1.7.1' && \
	conda clean --all
